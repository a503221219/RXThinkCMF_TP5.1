<?php

return [
    'rxthink' => [
        'name' => 'RXThinkCMF',
        'system_version' => '2.0.0',
        'build_version' => '201901010001',
        'copyright' => 'rxthink.cn',
        'url' => 'http://www.rxthink.cn',
    ],
];
